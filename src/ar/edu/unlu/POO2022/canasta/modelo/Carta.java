package ar.edu.unlu.POO2022.canasta.modelo;

public class Carta implements ICarta {
	private Numeros numero;
	private Palos palo;
	private int puntaje;
	
	public Carta(Numeros numero, Palos palo) {
		this.numero = numero;
		this.palo = palo;
		this.asignarPuntaje();
	}
	
	private void asignarPuntaje() {
		if (this.isComodin()){
			switch (numero) {
			case DOS:
				this.puntaje = 20;
				break;
			case JOKER:
				this.puntaje = 50;
				break;
			default:
				break;
			}
		}else
			if (numero.getValor()>=4 && numero.getValor()<=7) {
				this.puntaje = 5;
			}else {
				if (numero.getValor()>=8 && numero.getValor()<=13) {
					this.puntaje = 10;
				}else {
					if (numero.getValor()==1) {
						this.puntaje = 20;
					}else
						this.puntaje = 100;
				}	
			}
	}
 
	public Numeros getNumero() { 
		return numero;
	}
	
	public Palos getPalo() {
		return palo;
	}
	
	public boolean isComodin() {
		return (numero == Numeros.DOS || numero == Numeros.JOKER);
	}
	
	public boolean isJoker() {
		return (numero == Numeros.JOKER);
	}
	
	public boolean isTresRojo() {
		return ((numero == Numeros.TRES && palo == Palos.DIAMANTES) || (numero == Numeros.TRES && palo == Palos.CORAZONES));
	}
	
	public String getCartaInString() {
		String respuesta;
		if (!this.isJoker())
			respuesta = numero.getDescripcion() + palo.getpaloAbreviado();
		else
			respuesta = numero.getDescripcion();
		return respuesta;
	}
	
	public int getPuntaje() {
		return this.puntaje;
	}
	
	public boolean isTresNegro() {
		return ((numero == Numeros.TRES && palo == Palos.PICAS) || (numero == Numeros.TRES && palo == Palos.TREBOLES));
	}
	
}
