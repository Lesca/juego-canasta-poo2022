package ar.edu.unlu.POO2022.canasta.modelo;

import java.util.EnumSet;

public enum Palos {
	JOKER("JK"),
	CORAZONES("C"),
	DIAMANTES("D"),	
	TREBOLES("T"),
	PICAS("P");
	
	private String paloAbreviado;
	
	//Devuelve un tipo EnumSet que maneja enumerados
	public static EnumSet<Palos> getPalos() {
		EnumSet<Palos> e = EnumSet.complementOf(EnumSet.of(Palos.JOKER));
		return e;
	}
	
	private Palos(String paloAbreviado) {
		this.paloAbreviado = paloAbreviado;
	}
	
	public String getpaloAbreviado() {
		return this.paloAbreviado;
	}
	
}
