package ar.edu.unlu.POO2022.canasta.modelo;
import java.util.*;

public class Baraja {
	
	private Stack<Carta> baraja = new Stack<Carta>();
	private int cantidadDeCartas;
	
	
	public Baraja(int cantidadDeJuegosDeCartas) {
		this.generarBaraja(cantidadDeJuegosDeCartas);
	}
	
	private void generarBaraja(int cantidadDeJuegosDeCartas) {
		for (Numeros n : Numeros.getNumeros()) {
			for (Palos p: Palos.getPalos()) {
				baraja.push(new Carta(n,p));
			}
		}
		baraja.push(new Carta(Numeros.JOKER,Palos.JOKER));
		baraja.push(new Carta(Numeros.JOKER,Palos.JOKER));
		baraja.addAll(baraja);
		if (cantidadDeJuegosDeCartas == 4) {
			baraja.addAll(baraja);
		}
	}
	
	
	public Stack<Carta> getBaraja() {
		return this.baraja;
	}
	
	public void mezclarBaraja() {
		ArrayList<Carta> barajaAux = new ArrayList<Carta>(baraja);
		int r;
 		baraja = new Stack<Carta>();
 		Carta carta;
		while (!barajaAux.isEmpty()) {
			r = (int) Math.floor(Math.random() * barajaAux.size());
			carta = barajaAux.get(r);
			barajaAux.remove(r);
			baraja.push(carta);
		}
	}
}
