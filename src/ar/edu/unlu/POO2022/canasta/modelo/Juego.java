package ar.edu.unlu.POO2022.canasta.modelo;

import java.util.Queue;
import java.util.Stack;

import ar.edu.unlu.POO2022.canasta.utilidades.Observable;

public class Juego extends Observable {
	private Queue<Equipo> equipos;
	private Baraja baraja;
	private Pozo pozo;
	private int puntosParaFinalizar;
	private ModalidadDeEquipos modalidadDeEquipos;
	
	public Juego() {
		
	}
	
	//inicia la partida a jugarse con los equipos ya completados
	public void iniciarPartida(ModalidadDeEquipos modalidadDeEquipos, int puntosParaFinalizar) {
		this.modalidadDeEquipos = modalidadDeEquipos;
		this.puntosParaFinalizar = puntosParaFinalizar;
	}
	
	public void iniciarRonda() {
		if (this.modalidadDeEquipos == ModalidadDeEquipos.INDIVIDUAL || this.modalidadDeEquipos == ModalidadDeEquipos.DUOS) {
			this.baraja = new Baraja (2);
		}else
			this.baraja = new Baraja (4);
	}
	
	private void repartirCartas() {
		
	}
}
