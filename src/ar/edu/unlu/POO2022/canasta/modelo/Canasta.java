package ar.edu.unlu.POO2022.canasta.modelo;

import java.util.ArrayList;
import java.util.List;

public class Canasta {
	private List<Carta> canasta;
	private Numeros numeroDeCanasta;
	
	public Canasta(List<Carta> canasta) {
		if (isCanastaValida(canasta)) {
			this.canasta = canasta;
			for (Carta c: canasta) {
				if (!c.isComodin())
					this.numeroDeCanasta = c.getNumero();
			}
		}
	}
	
	public boolean agregarCarta(Carta carta) {
		boolean respuesta = false;
		if (!this.isCompleta() && carta.getNumero() == numeroDeCanasta && this.isCartaValida(carta)) {
			canasta.add(carta);
			respuesta = true;
		}
		return respuesta;
	}
	
	public boolean isCanastaValida(List<Carta> canasta) {
		boolean validez = true;
			
		return validez;
	}
	
	private boolean isCartaValida(Carta carta) {
		boolean respuesta = true;
		if (carta.isTresNegro() || carta.isTresRojo()) {
			respuesta = false;
		}
		return respuesta;
	}
	
	public int calcularPuntajeDeCanasta() {
		int puntaje = 0;
		for (Carta c: canasta) {
			puntaje = puntaje + c.getPuntaje();
		}
		return puntaje;
	}
	
	public List<Carta> getCanasta(){
		return this.canasta;
	}
	
	public boolean isCompleta() {
		return (canasta.size() == 7);
	}
	
	public boolean isLimpia() {
		boolean respuesta = true;
		for (Carta c: canasta) {
			respuesta = !c.isComodin();
		}
		return respuesta;
	}
	
	
	
}
