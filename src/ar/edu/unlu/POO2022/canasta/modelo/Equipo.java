package ar.edu.unlu.POO2022.canasta.modelo;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Equipo {
	private String nombre;
	//private HashMap<Integer, Jugador> jugadores;
	private List<Canasta> canastas;
	private int puntajeRonda;
	private int puntajeTotal;
	
	public Equipo(String nombre) {
		this.nombre = nombre;
		this.puntajeRonda = 0;
		this.puntajeTotal = 0;
	}
	
	public void sumarPuntajeTotal() {
		
	}
	
	public void sumarPuntajeRonda() {
		
	}
	
	public int getPuntajeTotal() {
		return this.puntajeTotal;
	}
	
	public int getPuntajeRonda() {
		return this.puntajeRonda;
	}
	
}
