package ar.edu.unlu.POO2022.canasta.modelo;

import java.util.Stack;

public class Pozo {
	private Stack<Carta> pozo;
	private boolean vulnerado;
	
	public Pozo(Stack<Carta> pozo) {
		this.vulnerado = false;
		this.pozo = pozo;
	}
	
	public Carta getUltimaCarta() {
		Carta c;
		return c = pozo.lastElement();
	}
	
	public Stack<Carta> getPozo() {
		Stack<Carta> p = null;
		if (!this.isBloqueado()) {
			p = this.pozo;
			this.vulnerado = false;
			this.pozo = new Stack<Carta>();
		}
		return p;
	}
	
	public boolean isVulnerado() {
		return vulnerado;
	}
	
	public void depositarCarta(Carta c) {
		pozo.push(c);
		if (c.isComodin()){
			this.vulnerado = true;
		}
	}
	
	public boolean isBloqueado() {
		boolean respuesta;
		return pozo.lastElement().isTresNegro();
	}
	
	
}
