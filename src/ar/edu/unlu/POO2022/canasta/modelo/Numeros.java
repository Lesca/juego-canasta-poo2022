package ar.edu.unlu.POO2022.canasta.modelo;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public enum Numeros {
	JOKER(0,"JK"),
	AS(1,"A"),
	DOS(2,"2"),
	TRES(3,"3"),
	CUATRO(4,"4"),
	CINCO(5,"5"),
	SEIS(6,"6"),
	SIETE(7,"7"),
	OCHO(8,"8"),
	NUEVE(9,"9"),
	DIEZ(10,"10"),
	J(11,"J"),
	Q(12,"Q"),
	K(13,"K");
	
	private String descripcion;
	private int valor;
	
	private Numeros(int valor,String descripcion) {
		this.descripcion = descripcion;
		this.valor = valor;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	public int getValor() {
		return this.valor;
	}
	
	public static EnumSet<Numeros> getNumeros() {
		EnumSet<Numeros> e = EnumSet.complementOf(EnumSet.of(Numeros.JOKER));
		return e;
	}
}
