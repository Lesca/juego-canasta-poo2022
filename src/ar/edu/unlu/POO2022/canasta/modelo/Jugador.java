package ar.edu.unlu.POO2022.canasta.modelo;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Jugador {
	private static int ID = 0;
	private int id;
	private String nombre;
	private List<Carta> mano;
	private List<Carta> honorificas;
	private Equipo equipo;
	
	public Jugador(String nombre) {
		this.nombre = nombre;
		this.mano = new ArrayList<Carta>();
		this.honorificas = new ArrayList<Carta>();
		this.id = ID ++;
	}
	
	public boolean setEquipo(Equipo equipo) {
		this.equipo = equipo;
		return true;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Carta> getMano(){
		return this.mano;
	}
	
	public void tomarCarta(Carta carta) {
		if (carta.isTresRojo())
			honorificas.add(carta);
		else
			mano.add(carta);
	}
	
	public int getId() {
		return this.id;
	}
	
	public Equipo getEquipo() {
		return this.equipo;
	}
	
	public boolean agruparCartas(List<Carta> mano) {
		//agregar comportamiento luego
		return true;
	}
	
	public boolean agregasrCartaACanasta(Carta carta) {
		//agregar comportamiento luego
		return true;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	
	
	public void ordenarMano() {
		mano.sort(Comparator.comparing(Carta::getNumero));
	}
	
}
