package ar.edu.unlu.POO2022.canasta.modelo;

public interface ICarta {
	public String getCartaInString();
	public Numeros getNumero();
	public Palos getPalo();
	public boolean isComodin();
	public boolean isJoker();
	public boolean isTresRojo();
}
