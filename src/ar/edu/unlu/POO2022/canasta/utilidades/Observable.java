package ar.edu.unlu.POO2022.canasta.utilidades;

import java.util.List;

public abstract class Observable {
	private List<Observer> observers;
	
	public void notificar(Eventos evento, Observable observado) {
		for (Observer o: observers) {
			o.actualizar(evento, observado);
		}
	}
	
	public void addObserver(Observer observador) {
		observers.add(observador);
	}
}
