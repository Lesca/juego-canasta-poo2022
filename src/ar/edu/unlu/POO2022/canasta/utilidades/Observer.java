package ar.edu.unlu.POO2022.canasta.utilidades;

public interface Observer {
	public void actualizar(Eventos evento, Observable observado);
}
