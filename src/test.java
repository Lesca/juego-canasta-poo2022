import java.util.Stack;

import ar.edu.unlu.POO2022.canasta.modelo.*;

public class test {
	
	public static void main(String[] args) {
		Baraja b = new Baraja(2);
		Stack<Carta> s =  b.getBaraja();
		Carta c;
		System.out.println("Cantidad de cartas: "+ s.size());
		while (!s.empty()) {
			c = s.pop();
			System.out.println(c.getNumero() + "  " + c.getPalo());
		}
		System.out.println("-----------------------------------------------");
		System.out.println("-----------------------------------------------");
		System.out.println("-----------------------------------------------");
		b = new Baraja(2);
		b.mezclarBaraja();
		s = b.getBaraja();
		System.out.println(s.size());
		//while (!s.empty()) {
		//	c = s.pop();
		//	System.out.println(c.getCartaInString()+ "    Puntaje: "+ c.getPuntaje());
		//}
		System.out.println("-----------------------------------------------");
		System.out.println("-----------------------------------------------");
		System.out.println("-----------------------------------------------");
		Jugador j = new Jugador("Pepito");
		for (int i = 0; i <=14; i++) {
			j.tomarCarta(s.pop());
		}
		j.ordenarMano();
		for (Carta cAux: j.getMano()) {
			System.out.println(cAux.getCartaInString());
		}
	}
}
